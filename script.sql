USE DB_DONGHO

/****** Object:  Table chitietdonhang    Script Date: 05-Jun-19 6:35:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE chitietdonhang(
	ma_ddh int NOT NULL,
	ma_sp int NOT NULL,
	soluong int NULL,
	dongia int NULL,
 CONSTRAINT PK_chitietdonhang PRIMARY KEY CLUSTERED 
(
	ma_ddh ASC,
	ma_sp ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
) ON PRIMARY


/****** Object:  Table dondathang    Script Date: 05-Jun-19 6:35:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE dondathang(
	ma_ddh int NOT NULL,
	ma_kh int NOT NULL,
	nguoinhan nvarchar(50) NULL,
	diachigiao nvarchar(50) NULL,
	sodt nvarchar(11) NULL,
	tinhtrang nvarchar(50) NULL,
 CONSTRAINT PK_dondathang PRIMARY KEY CLUSTERED 
(
	ma_ddh ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
) ON PRIMARY


/****** Object:  Table khachhang    Script Date: 05-Jun-19 6:35:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE khachhang(
	ma_kh int NOT NULL,
	hoten nvarchar(50) NULL,
	username nvarchar(50) NULL,
	password nvarchar(50) NULL,
	diachi nvarchar(50) NULL,
	sodt nvarchar(50) NULL,
	role nvarchar(10) NULL,
 CONSTRAINT PK_khachhang PRIMARY KEY CLUSTERED 
(
	ma_kh ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
) ON PRIMARY


/****** Object:  Table nhasanxuat    Script Date: 05-Jun-19 6:35:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE nhasanxuat(
	ma_nsx int NOT NULL,
	ten_nsx nvarchar(50) NOT NULL,
	thongtin nvarchar(200) NULL,
	lo varchar(20) NULL,
	website varchar(20) NULL,
 CONSTRAINT PK_nhasanxuat PRIMARY KEY CLUSTERED 
(
	ma_nsx ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
) ON PRIMARY


SET ANSI_PADDING OFF

/****** Object:  Table sanpham    Script Date: 05-Jun-19 6:35:59 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE sanpham(
	ma_sp int NOT NULL,
	ma_nsx int NOT NULL,
	ten_sp nvarchar(50) NULL,
	dongia int NULL,
	thongtin nvarchar(500) NULL,
	hinhanh nvarchar(50) NULL,
	baohanh nvarchar(100) NULL,
	tinhtrang nvarchar(50) NULL,
 CONSTRAINT PK_sanpham PRIMARY KEY CLUSTERED 
(
	ma_sp ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY
) ON PRIMARY


ALTER TABLE chitietdonhang  WITH CHECK ADD  CONSTRAINT FK_chitietdonhang_dondathang FOREIGN KEY(ma_ddh)
REFERENCES dondathang (ma_ddh)
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE chitietdonhang CHECK CONSTRAINT FK_chitietdonhang_dondathang

ALTER TABLE chitietdonhang  WITH CHECK ADD  CONSTRAINT FK_chitietdonhang_sanpham FOREIGN KEY(ma_sp)
REFERENCES sanpham (ma_sp)
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE chitietdonhang CHECK CONSTRAINT FK_chitietdonhang_sanpham

ALTER TABLE dondathang  WITH CHECK ADD  CONSTRAINT FK_dondathang_khachhang FOREIGN KEY(ma_kh)
REFERENCES khachhang (ma_kh)
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE dondathang CHECK CONSTRAINT FK_dondathang_khachhang

ALTER TABLE sanpham  WITH CHECK ADD  CONSTRAINT FK_sanpham_nhasanxuat FOREIGN KEY(ma_nsx)
REFERENCES nhasanxuat (ma_nsx)
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE sanpham CHECK CONSTRAINT FK_sanpham_nhasanxuat

