package com.example.watchshop.Inventory;

import com.example.watchshop.Catalog.WatchCatalog;

import org.salespointframework.catalog.ProductIdentifier;
import org.salespointframework.core.DataInitializer;
import org.salespointframework.inventory.Inventory;
import org.salespointframework.inventory.InventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.salespointframework.quantity.Quantity;

@Component
@Order(20)
/**
 * InventoryInitializer
 */
public class InventoryInitializer implements DataInitializer {

    private final Inventory<InventoryItem> inventory;
    private final WatchCatalog watchCatalog;

    @Autowired
    InventoryInitializer(WatchCatalog watchCatalog, Inventory<InventoryItem> inventory) {

        this.watchCatalog = watchCatalog;
        this.inventory = inventory;
    }

    @Override
    public void initialize() {

        watchCatalog.findAll().forEach(watch -> {
            inventory.findByProduct(watch).orElseGet(() -> inventory.save(new InventoryItem(watch, Quantity.of(10))));
        });

    }

    public void deleteInventoryItem(ProductIdentifier productIdentifier) {
        for (InventoryItem inventoryItem : inventory.findAll()) {
            if (inventoryItem.getProduct().getId().equals(productIdentifier)) {
                inventory.delete(inventoryItem);
            }
        }
    }

}