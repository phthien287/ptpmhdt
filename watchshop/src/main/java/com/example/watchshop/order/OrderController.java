package com.example.watchshop.order;

import java.util.Optional;

import com.example.watchshop.Catalog.Watch;

import org.salespointframework.order.Cart;
import org.salespointframework.order.Order;
import org.salespointframework.order.OrderManager;
import org.salespointframework.order.OrderStatus;
import org.salespointframework.payment.Cash;
import org.salespointframework.quantity.Quantity;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.web.LoggedIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
/**
 * OrderController
 */
@PreAuthorize("isAuthenticated()")
@SessionAttributes("cart")
public class OrderController {

    private final OrderManager<Order> orderManager;

    @Autowired
    OrderController(OrderManager<Order> orderManager) {
        this.orderManager = orderManager;
    }

    @ModelAttribute("cart")
    Cart initializeCart() {
        return new Cart();
    }

    @PostMapping("/cart")
    String addWatch(@RequestParam("pid") Watch watch, @RequestParam("number") int number, @ModelAttribute Cart cart) {

        int amount = number <= 0 || number >= 5 ? 1 : number;

        cart.addOrUpdateItem(watch, Quantity.of(amount));

        switch (watch.getType()) {
        case MALE:
            return "redirect:male";
        case FEMALE:
        default:
            return "redirect:female";
        }
    }

    @GetMapping("/cart")
    String basket() {
        return "cart";
    }

    @PostMapping("/checkout")
    String buy(@ModelAttribute Cart cart, @LoggedIn Optional<UserAccount> userAccount) {
        return userAccount.map(account -> {
            Order order = new Order(account, Cash.CASH);

            cart.addItemsTo(order);

            orderManager.payOrder(order);
            orderManager.completeOrder(order);

            cart.clear();

            return "redirect:/";
        }).orElse("redirect:/cart");
    }

    @GetMapping("/orders")
    @PreAuthorize("hasAuthority('ADMIN')")
    String order(Model model) {

        model.addAttribute("ordersCompleted", orderManager.findBy(OrderStatus.COMPLETED));

        return "orders";
    }
}