package com.example.watchshop.Customers;

// import org.salespointframework.useraccount.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.stereotype.Repository;

// @Repository("customerRepository")
/**
 * CustomerRepository
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    // Customer findByUserAccount(UserAccount userAccount);
}