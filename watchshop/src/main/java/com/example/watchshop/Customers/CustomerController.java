package com.example.watchshop.Customers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
/**
 * CustomerController
 */
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/customers")
    @PreAuthorize("hasAuthority('ADMIN')")
    String customers(Model model) {
        model.addAttribute("customerList", customerService.getAlllCustomers());
        return "customers";
    }

    @GetMapping("/register")
    public String register(Model model, RegistrationForm registrationForm) {
        model.addAttribute("registrationForm", registrationForm);

        return "register";
    }

    @PostMapping("/register")
    public String registerNew(@Valid RegistrationForm registrationForm, Errors result) {
        // TODO: process POST request
        if (result.hasErrors()) {
            return "register";
        }
        customerService.createCustomer(registrationForm);
        return "redirect:/";
    }

}