package com.example.watchshop.Customers;

import javax.validation.constraints.NotEmpty;

/**
 * RegistrationForm
 */
public interface RegistrationForm {

    @NotEmpty(message = "{RegistrationForm.username.NotEmpty}")
    String getUsername();

    @NotEmpty(message = "{RegistrationForm.password.NotEmpty}")
    String getPassword();

    String getAddress();

}