package com.example.watchshop.Customers;

import javax.persistence.*;
import org.salespointframework.useraccount.UserAccount;

import lombok.Data;

@Data
@Entity
/**
 * Customer
 */
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private String address;

    @OneToOne
    private UserAccount userAccount;

    @SuppressWarnings("unused")
    private Customer() {

    }

    public Customer(String address, UserAccount userAccount) {
        this.address = address;
        this.userAccount = userAccount;
    }
}