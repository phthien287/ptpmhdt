package com.example.watchshop.Customers;

// import java.util.List;

import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service("customerService")
/**
 * CustomerService
 */
@Transactional
public class CustomerService {

    private CustomerRepository customerRepository;
    private UserAccountManager userAccountManager;

    @Autowired
    CustomerService(CustomerRepository customerRepository, UserAccountManager userAccountManager) {
        this.customerRepository = customerRepository;
        this.userAccountManager = userAccountManager;
    }

    public Streamable<Customer> getAlllCustomers() {
        return Streamable.of(customerRepository.findAll());
    }

    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer, Long id) {
        Customer newCustomer = customerRepository.findById(id).orElse(null);
        if (newCustomer != null) {

            newCustomer.setAddress(customer.getAddress());
        }
        final Customer updatedCustomer = customerRepository.save(newCustomer);
        return updatedCustomer;
    }

    public Boolean deleteCustomer(Long id) {
        Customer delCustomer = customerRepository.findById(id).orElse(null);
        if (delCustomer != null) {
            customerRepository.delete(delCustomer);
            return true;
        }
        return false;
    }

    public Customer createCustomer(RegistrationForm registrationForm) {
        Assert.notNull(registrationForm, "Registration form must not be null");
        UserAccount userAccount = userAccountManager.create(registrationForm.getUsername(),
                registrationForm.getPassword(), Role.of("CUSTOMER"));

        return customerRepository.save(new Customer(registrationForm.getAddress(), userAccount));

    }

}