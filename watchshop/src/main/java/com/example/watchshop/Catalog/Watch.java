package com.example.watchshop.Catalog;

import javax.persistence.Entity;

import org.javamoney.moneta.Money;
import org.salespointframework.catalog.Product;

import lombok.Data;

@Data
@Entity
/**
 * Watch
 */
public class Watch extends Product {

    public static enum WatchType {
        MALE, FEMALE;
    }

    private WatchType type;
    private String image;

    private String description;

    @SuppressWarnings("unused")
    private Watch() {

    }

    public Watch(String name, Money price, String image, String description, WatchType type) {
        super(name, price);

        this.image = image;
        this.description = description;
        this.type = type;
    }

}