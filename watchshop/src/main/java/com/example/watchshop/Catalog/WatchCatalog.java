package com.example.watchshop.Catalog;

import com.example.watchshop.Catalog.Watch.WatchType;

import org.salespointframework.catalog.Catalog;
import org.springframework.data.domain.Sort;

/**
 * WatchCatalog
 */
public interface WatchCatalog extends Catalog<Watch> {

    static final Sort DEFAULT_SORT = Sort.by("productIdentifier").descending();

    Iterable<Watch> findByType(WatchType type, Sort sort);

    default Iterable<Watch> findByType(WatchType type) {
        return findByType(type, DEFAULT_SORT);
    }

}