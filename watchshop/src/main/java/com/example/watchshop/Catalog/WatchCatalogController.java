package com.example.watchshop.Catalog;

import java.util.Optional;

import com.example.watchshop.Catalog.Watch.WatchType;

import org.salespointframework.inventory.Inventory;
import org.salespointframework.inventory.InventoryItem;
import org.salespointframework.quantity.Quantity;
// import org.salespointframework.time.BusinessTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/catalog")
/**
 * WatchCatalogController
 */
public class WatchCatalogController {

    private static final Quantity NONE = Quantity.of(0);

    private final Inventory<InventoryItem> inventory;
    private final WatchCatalog watchCatalog;
    // private final BusinessTime businessTime;

    @Autowired
    WatchCatalogController(WatchCatalog watchCatalog, Inventory<InventoryItem> inventory) {

        this.watchCatalog = watchCatalog;
        // this.businessTime = businessTime;
        this.inventory = inventory;
    }

    @GetMapping("/Male")
    String maleCatalog(Model model) {
        model.addAttribute("catalog", watchCatalog.findByType(WatchType.MALE));
        model.addAttribute("title", "catalog.male.title");

        return "catalog";
    }

    @GetMapping("/Female")
    String femaleCatalog(Model model) {
        model.addAttribute("catalog", watchCatalog.findByType(WatchType.FEMALE));
        model.addAttribute("title", "catalog.female.title");

        return "catalog";
    }

    @GetMapping(value = "/Watch/{watch}")
    public String detail(@PathVariable Watch watch, Model model) {

        Optional<InventoryItem> item = inventory.findByProductIdentifier(watch.getId());
        Quantity quantity = item.map(InventoryItem::getQuantity).orElse(NONE);

        model.addAttribute("watch", watch);
        model.addAttribute("quantity", quantity);
        model.addAttribute("orderable", quantity.isGreaterThan(NONE));

        return "detail";
    }

}