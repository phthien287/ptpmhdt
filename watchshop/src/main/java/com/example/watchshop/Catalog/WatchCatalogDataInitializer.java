package com.example.watchshop.Catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@Order(20)
/**
 * WatchDataInitializer
 */
public class WatchCatalogDataInitializer {

    private final WatchCatalog watchCatalog;

    @Autowired
    WatchCatalogDataInitializer(WatchCatalog watchCatalog) {
        Assert.notNull(watchCatalog, "Watch Catalog must not be null !");
        this.watchCatalog = watchCatalog;
    }

}