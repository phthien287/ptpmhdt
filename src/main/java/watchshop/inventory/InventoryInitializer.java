package watchshop.inventory;

import watchshop.catalog.WatchCatalog;

import org.salespointframework.core.DataInitializer;
import org.salespointframework.inventory.Inventory;
import org.salespointframework.inventory.InventoryItem;
import org.salespointframework.quantity.Quantity;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@Order(20)
class InventoryInitializer implements DataInitializer {

	private final Inventory<InventoryItem> inventory;
	private final WatchCatalog videoCatalog;

	InventoryInitializer(Inventory<InventoryItem> inventory, WatchCatalog videoCatalog) {

		Assert.notNull(inventory, "Inventory must not be null!");
		Assert.notNull(videoCatalog, "VideoCatalog must not be null!");

		this.inventory = inventory;
		this.videoCatalog = videoCatalog;
	}

	@Override
	public void initialize() {

		videoCatalog.findAll().forEach(disc -> {

			inventory.findByProduct(disc) //
					.orElseGet(() -> inventory.save(new InventoryItem(disc, Quantity.of(10))));
		});
	}
}
