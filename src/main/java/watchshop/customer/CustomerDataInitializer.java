package watchshop.customer;

import java.util.Arrays;

import org.salespointframework.core.DataInitializer;
import org.salespointframework.useraccount.Role;
import org.salespointframework.useraccount.UserAccount;
import org.salespointframework.useraccount.UserAccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@Order(10)
class CustomerDataInitializer implements DataInitializer {

	private static final Logger LOG = LoggerFactory.getLogger(CustomerDataInitializer.class);

	private final UserAccountManager userAccountManager;
	private final CustomerRepository customerRepository;

	CustomerDataInitializer(UserAccountManager userAccountManager, CustomerRepository customerRepository) {

		Assert.notNull(customerRepository, "CustomerRepository must not be null!");
		Assert.notNull(userAccountManager, "UserAccountManager must not be null!");

		this.userAccountManager = userAccountManager;
		this.customerRepository = customerRepository;
	}

	@Override
	public void initialize() {

		if (userAccountManager.findByUsername("admin").isPresent()) {
			return;
		}

		LOG.info("Creating default users and customers.");
		UserAccount adminAccount = userAccountManager.create("boss", "123", Role.of("BOSS"));
		// userAccountManager.save(adminAccount);
		userAccountManager.delete(adminAccount);

		// Role customerRole = Role.of("CUSTOMER");

		// UserAccount ua1 = userAccountManager.create("thao", "123", customerRole);
		// UserAccount ua2 = userAccountManager.create("tuan", "123", customerRole);
		// UserAccount ua3 = userAccountManager.create("nga", "123", customerRole);
		// UserAccount ua4 = userAccountManager.create("uy", "123", customerRole);

		// Customer c1 = new Customer(ua1, "Quy nhon");
		// Customer c2 = new Customer(ua2, "Quy nhon");
		// Customer c3 = new Customer(ua3, "Quy nhon");
		// Customer c4 = new Customer(ua4, "Quy nhon");

		// customerRepository.saveAll(Arrays.asList(c1, c2, c3, c4));
		customerRepository.deleteAll();
	}
}
