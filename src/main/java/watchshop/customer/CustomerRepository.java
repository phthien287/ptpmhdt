package watchshop.customer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
interface CustomerRepository extends CrudRepository<Customer, Long> {
}
