package watchshop.catalog;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.validation.Valid;

import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.salespointframework.inventory.Inventory;
import org.salespointframework.inventory.InventoryItem;
import org.salespointframework.quantity.Quantity;
import org.salespointframework.time.BusinessTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import watchshop.catalog.Watch.WatchType;

@Controller
class CatalogController {

	private static final Quantity NONE = Quantity.of(0);

	private final WatchCatalog catalog;
	private final Inventory<InventoryItem> inventory;
	private final BusinessTime businessTime;

	CatalogController(WatchCatalog videoCatalog, Inventory<InventoryItem> inventory, BusinessTime businessTime) {

		this.catalog = videoCatalog;
		this.inventory = inventory;
		this.businessTime = businessTime;
	}

	@GetMapping("/dvds")
	String dvdCatalog(Model model) {

		model.addAttribute("catalog", catalog.findByType(WatchType.DVD));
		model.addAttribute("title", "catalog.dvd.title");

		return "catalog";
	}

	@GetMapping("/blurays")
	String blurayCatalog(Model model) {

		model.addAttribute("catalog", catalog.findByType(WatchType.BLURAY));
		model.addAttribute("title", "catalog.bluray.title");

		return "catalog";
	}

	@GetMapping("/disc/{disc}")
	String detail(@PathVariable Watch disc, Model model) {

		Optional<InventoryItem> item = inventory.findByProductIdentifier(disc.getId());
		Quantity quantity = item.map(InventoryItem::getQuantity).orElse(NONE);

		model.addAttribute("disc", disc);
		model.addAttribute("quantity", quantity);
		model.addAttribute("orderable", quantity.isGreaterThan(NONE));

		return "detail";
	}

	@PostMapping("/disc/{disc}/comments")
	public String comment(@PathVariable Watch disc, @Valid CommentAndRating payload) {

		disc.addComment(payload.toComment(businessTime.getTime()));
		catalog.save(disc);

		return "redirect:/disc/" + disc.getId();
	}

	interface CommentAndRating {

		@NotEmpty
		String getComment();

		@Range(min = 1, max = 5)
		int getRating();

		default Comment toComment(LocalDateTime time) {
			return new Comment(getComment(), getRating(), time);
		}
	}
}
