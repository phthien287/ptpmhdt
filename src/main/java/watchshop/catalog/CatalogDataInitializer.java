package watchshop.catalog;

import static org.salespointframework.core.Currencies.*;

import watchshop.catalog.Watch.WatchType;

import org.javamoney.moneta.Money;
import org.salespointframework.core.DataInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@Order(20)
class CatalogDataInitializer implements DataInitializer {

	private static final Logger LOG = LoggerFactory.getLogger(CatalogDataInitializer.class);

	private final WatchCatalog watchCatalog;

	CatalogDataInitializer(WatchCatalog watchCatalog) {

		Assert.notNull(watchCatalog, "WatchCatalog must not be null!");

		this.watchCatalog = watchCatalog;
	}

	@Override
	public void initialize() {

		if (watchCatalog.findAll().iterator().hasNext()) {
			return;
		}

		LOG.info("Creating default catalog entries.");

		watchCatalog.save(new Watch("Last Action Hero", "lac", Money.of(100, EURO), "Äktschn/Comedy", WatchType.DVD));
		watchCatalog.save(new Watch("Back to the Future", "bttf", Money.of(9.99, EURO), "Sci-Fi", WatchType.DVD));
		watchCatalog.save(new Watch("Fido", "fido", Money.of(9.99, EURO), "Comedy/Drama/Horror", WatchType.DVD));
		watchCatalog.save(new Watch("Super Fuzz", "sf", Money.of(9.99, EURO), "Action/Sci-Fi/Comedy", WatchType.DVD));
		watchCatalog.save(new Watch("Armour of God II: Operation Condor", "aog2oc", Money.of(14.99, EURO),
				"Action/Adventure/Comedy", WatchType.DVD));
		watchCatalog.save(
				new Watch("Persepolis", "pers", Money.of(14.99, EURO), "Animation/Biography/Drama", WatchType.DVD));
		watchCatalog.save(
				new Watch("Hot Shots! Part Deux", "hspd", Money.of(9999.0, EURO), "Action/Comedy/War", WatchType.DVD));
		watchCatalog.save(new Watch("Avatar: The Last Airbender", "tla", Money.of(19.99, EURO),
				"Animation/Action/Adventure", WatchType.DVD));

		watchCatalog
				.save(new Watch("Secretary", "secretary", Money.of(6.99, EURO), "Political Drama", WatchType.BLURAY));
		watchCatalog.save(new Watch("The Godfather", "tg", Money.of(19.99, EURO), "Crime/Drama", WatchType.BLURAY));
		watchCatalog.save(
				new Watch("No Retreat, No Surrender", "nrns", Money.of(29.99, EURO), "Martial Arts", WatchType.BLURAY));
		watchCatalog.save(new Watch("The Princess Bride", "tpb", Money.of(39.99, EURO), "Adventure/Comedy/Family",
				WatchType.BLURAY));
		watchCatalog.save(new Watch("Top Secret!", "ts", Money.of(39.99, EURO), "Comedy", WatchType.BLURAY));
		watchCatalog.save(new Watch("The Iron Giant", "tig", Money.of(34.99, EURO), "Animation/Action/Adventure",
				WatchType.BLURAY));
		watchCatalog.save(
				new Watch("Battle Royale", "br", Money.of(19.99, EURO), "Action/Drama/Thriller", WatchType.BLURAY));
		watchCatalog.save(new Watch("Oldboy", "old", Money.of(24.99, EURO), "Action/Drama/Thriller", WatchType.BLURAY));
		watchCatalog.save(new Watch("Bill & Ted's Excellent Adventure", "bt", Money.of(29.99, EURO),
				"Adventure/Comedy/Family", WatchType.BLURAY));
	}
}
